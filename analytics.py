import pandas as pd, numpy as np
import seaborn as sns

from matplotlib import pyplot as plt

from sklearn.metrics import explained_variance_score, mean_squared_error, r2_score, mean_squared_log_error
from sklearn.metrics import precision_score, accuracy_score, recall_score, roc_auc_score
from sklearn.metrics import accuracy_score, cohen_kappa_score, balanced_accuracy_score

### Definitions
# Class color helps emphasise the printed output
class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

# Description:
# cv results present the results of the cross validation of sklearn for specific metrics. 
# It prints the performance results, visuals for hyperparameter tuning and return the results.
# Input: 
#   model: the trained crossvalidated sklearn model
#   metric_names: list of metrics used
# Output:
#   results_df: scores of all metrics and their standard deviation for each combination of the parameter space.
def cv_results(model, metric_names):
    
    # get parameters 
    param_df = pd.DataFrame(model.cv_results_['params'])
    
    # create a list of metrics
    metric_name_list = [m for m in metric_names]
    
    # for each metric append the test scores and their standard deviations to the score df . For rmse take square root of the complement
    scores_df = pd.DataFrame()
    for m in metric_name_list:
        result_key = 'mean_test_' + m
        result_std_key = 'std_test_' + m
        if m == 'rmse':
            scores = pd.Series(np.sqrt(-model.cv_results_[result_key]), name = m)
            scores_std = pd.Series(np.sqrt(model.cv_results_[result_std_key]), name = m + '_sd')
        else:
            scores = pd.Series(model.cv_results_[result_key], name = m)
            scores_std = pd.Series(model.cv_results_[result_std_key], name = m + '_sd')
        scores_df = pd.concat([scores_df, scores, scores_std], axis = 1)
    
    # append results to parameters
    results_df = pd.concat([param_df, scores_df], axis = 1)
    
    # print results
    best_param_str = ''
    for p in param_df.columns:
        best_param_str = best_param_str + p + ' : ' + str(results_df.loc[model.best_index_, p]) + '\n'
    best_score_str = ''
    for m in metric_names:
        best_score_str = best_score_str + m + ' : ' + str(round(results_df.loc[model.best_index_, m],2)) + '\n'
    print('Best model in training')
    print('Parameters: ')
    print(best_param_str)
    print('Scores')
    print(best_score_str)

    # print line plots for each parameter and metric
    sns.set(font_scale=2)
    fig, ax = plt.subplots(nrows = len(param_df.columns), ncols = len(metric_name_list), figsize = (30, 30))
    if len(param_df.columns) > 1 and len(metric_names) > 1: 
        for i in range(len(param_df.columns)):
            j = 0
            for m in metric_name_list:
                sns.lineplot(x = param_df.columns[i], y = m, data = results_df, ax = ax[i][j])
                j+=1
    else:
        if len(param_df.columns) == 1:
            for i in range(len(metric_name_list)):
                sns.lineplot(x = param_df.columns[0], y = metric_name_list[i], data = results_df, ax = ax[i])
        else:
            for i in range(len(param_df.columns)):
                sns.lineplot(x = param_df.columns[i], y = metric_name_list[0], data = results_df, ax = ax[i])
    fig.suptitle('Hyperparameter Tuning: ')
    
    return(results_df)

# Description:
# This function prints the results of the regression model on test or validation set. It uses r2, explained variance and rmse from sklearn 
# to evaluate the performance of the regression model. The prediction is plotted against the target variable on a plot ordered by the target
# variable in ascending order. If a semgentation is used, individual performance metrics and plots are produced for each segment. 
# Input: 
#   pred: the estimated target variable. 
#   y: The target variable.
#   segmentation: If a segmentation is used for the analysis, is the categorical variable signifying the segment. It should have the same index 
#                 as y and X and pred. Defaults to None
#   y_name: Pretty name of the target variable to used in the report. Defaults to 'target'
#   description: Description of the model. Used to be returned in the result dictionary so it can be used in experiments. Defaults to 'model'
# Output:
#   result_dict: dictionary with a description of the model and the performance metrics
def test_results_regression(pred, y, segmentation = None, y_name = "Target",  description = 'Model'):
       
    # create series of estimations and observations
    pred_s = pd.Series(pred, name = 'Estimated', index = y.index)
    obs_s = pd.Series(y, name = 'Observed', index = y.index)

    # print test restults
    print('Performance in test set:')
    print('R squared: %.2f \nExplained variance: %.2f \nRMSE: %.2f \n' 
      % (r2_score(y_true = obs_s, y_pred = pred_s),
         explained_variance_score(y_true = obs_s, y_pred = pred_s),
         mean_squared_error(y_true =obs_s, y_pred = pred_s, squared = False)))
    # create result dict
    result_dict = {'description' : description,
                   'r2_score' : r2_score(y_true = obs_s, y_pred = pred),
                   'explained_variance' : explained_variance_score(y_true = obs_s, y_pred = pred_s),
                   'rmse' : mean_squared_error(y_true = obs_s, y_pred = pred_s, squared = False)}

    # setting the correct index name for crosstabl
    if y.index.name is None:
        index_name = 'index'
    else:
        index_name = y.index.name
    
    # if there is segmentation provided, it prints separate results for each segment and adds segmentation to an evaluation dataframe 
    # of predictions and actuals
    if segmentation is not None:
        # index vars will be used in melt. Adds segmentation
        index_vars = [index_name, segmentation.name]
        eval_df = pd.concat([obs_s, pred_s, segmentation], axis = 1)
        # print test result for each segment
        print('Performance for each segment: ')
        for l in eval_df[segmentation.name].unique():
            print('Segment: ', l)
            label_df = eval_df[eval_df[segmentation.name] == l]
            result_dict[l + '_rmse'] = mean_squared_error(y_true = label_df['Observed'], 
                                                          y_pred = label_df['Estimated'], 
                                                          squared = False)
            print('R squared: %.2f \nExplained variance: %.2f \nRMSE: %.2f \n' 
                % (r2_score(y_true = label_df['Observed'], y_pred = label_df['Estimated']),
                   explained_variance_score(y_true = label_df['Observed'], y_pred = label_df['Estimated']),
                   mean_squared_error(y_true = label_df['Observed'], y_pred = label_df['Estimated'], squared = False)))
    else:
        # construct evaluation df of predictions and actuals without the segmentation
        eval_df = pd.concat([obs_s, pred_s], axis = 1)
        index_vars = [index_name]
    
    # melt the table so it can be grouped scores by observed and actuals
    eval_df = eval_df.sort_values('Observed').reset_index()
    #eval_df['index'] = eval_df['index'].astype(str) 

    eval_tab = eval_df.melt(id_vars = index_vars,
                            value_vars = ['Observed', 'Estimated'], 
                            value_name = y_name, 
                            ignore_index = False)
    
    # create plot
    sns.set(font_scale = 1)
    
    
    eval_df['difference'] = eval_df['Observed'] - eval_df['Estimated']
    #fig, axes = plt.subplots(nrows = 2, ncols = 1)
    # if there is segmentation create one plot for each segment
    if segmentation is not None:
        p = sns.relplot(x = eval_tab.index, y = y_name, data = eval_tab, size = 'variable', size_order = ['Observed', 'Estimated'],
                    kind = 'scatter', hue = 'variable',col = segmentation.name,
                    facet_kws=dict(sharey=False, sharex = False)) \
                    .set(xlabel = None, xticklabels = [])
        plt.show()
        p = sns.relplot( 
            x = 'Estimated', y = 'Observed', data = eval_df, hue = 'difference', kind = 'scatter', 
            col = segmentation.name,facet_kws=dict(sharey=False, sharex = False) 
        )
        plt.show()
    else:
        # create one overall plot
        p = sns.relplot(x = eval_tab.index, y = y_name, data = eval_tab, size = 'variable', size_order = ['Observed', 'Estimated'],
                    kind = 'scatter', hue = 'variable').set(xlabel = None, xticklabels = [])
        plt.show()
        p = sns.relplot( 
            x = 'Estimated', y = 'Observed', data = eval_df, hue = 'difference', kind = 'scatter')
        plt.show()
    return(result_dict)

# Description:
# This function prints the results of the binary classification model on test or validation set. 
# It uses accuracy, precision, recall and area under curve from sklearn library to evaluate the performance of the binary classification model.
# A heatmap of the confusion matrix between actuals and predictions if provided together with the perfomance metrix 
# Input: 
#   pred: the predicted target variable. 
#   y: The target variable.
#   positive: signify the positive class for precision and recall. Defaults to 1.
#   description: Description of the model. Used to be returned in the result dictionary so it can be used in experiments. Defaults to 'model'
# Output:
#   result_dict: dictionary with a description of the model and the performance metrics
def test_results_binary(pred, y, positive = 1, description = 'Model'):
   
    scores = {
        'accuracy' : accuracy_score(y_true = y, y_pred = pred),
        'precision' : precision_score(y_true = y, y_pred = pred, pos_label = positive),
        'recall' : recall_score(y_true = y, y_pred = pred, pos_label = positive),
        'auc' : roc_auc_score(y_true = y, y_score = pred)
    }
    # print performance metrics
    score_str = ''
    for metric in scores.keys():
        score_str = score_str + metric + ' : ' + str(round(scores[metric],2)) + '\n'
    print('Performance on test set: ')
    print(score_str)
    
    # create result dictionary
    result_dict = {
        'description' : description,
        'accuracy' : accuracy_score(y_true = y, y_pred = pred),
        'precision' : precision_score(y_true = y, y_pred = pred, pos_label = positive),
        'recall' : recall_score(y_true = y, y_pred = pred, pos_label = positive),
        'auc' : roc_auc_score(y_true = y, y_score = pred)
    }
    
    # create series of predictions and actuals
    pred_s = pd.Series(pred, name = 'Prediction', index = y.index)
    obs_s = pd.Series(y, name = 'Original')

    # create evaluation df with actuals and predictions and create confusion matrix
    eval_df = pd.concat([pred_s, obs_s], axis = 1)
    eval_df['val'] = 1
    # confusion matrix
    cm = eval_df.pivot_table(index = 'Original', columns = 'Prediction', values = 'val', aggfunc = np.sum)
    
    # print heatmap
    sns.set(font_scale=1)
    plt.figure(figsize = (10,5))
    sns.heatmap(cm, linewidths = 0.1, annot = True, fmt = 'd').set_title('Confusion Matrix')

    return(result_dict)

# Description:
# This function prints the results of the multiclass classification model on test or validation set. 
# It uses accuracy, cohen's kappa and balanced_accuracy from sklearn library to evaluate the performance of the multiclass classification model.
# A heatmap of the confusion matrix between actuals and predictions if provided together with the perfomance metrix 
# Input: 
#   pred: the predicted target variable. 
#   y: The target variable.
#   description: Description of the model. Used to be returned in the result dictionary so it can be used in experiments. Defaults to 'model'
# Output:
#   result_dict: dictionary with a description of the model and the performance metrics
def test_results_multiclass(pred, y, description = 'Model'):
    
    scores = {
        'accuracy' : accuracy_score(y_true = y, y_pred = pred), 
        'kappa' : cohen_kappa_score(y1 = y, y2 = pred), 
        'balanced_accuracy' : balanced_accuracy_score(y_true = y, y_pred = pred)
    }
    # print performance metrics
    score_str = ''
    for metric in scores.keys():
        score_str = score_str + metric + ' : ' + str(round(scores[metric],2)) + '\n'
    print('Performance on test set: ')
    print(score_str)
    
    # create result dictionary
    result_dict = {
        'description' : description,
        'accuracy' : accuracy_score(y_true = y, y_pred = pred),
        'kappa' : cohen_kappa_score(y1 = y, y2 = pred),
        'balanced_accuracy' :  balanced_accuracy_score(y_true = y, y_pred = pred)
    }
    
    # create series of predictions and actuals
    pred_s = pd.Series(pred, name = 'Prediction', index = y.index)
    obs_s = pd.Series(y, name = 'Original')
    
    # print frequencies of predicted and actual class
    print('Prediction:')
    print(pred_s.value_counts())
    print('Actuals:')
    print(obs_s.value_counts())
 
    # create evaluation df with actuals and predictions and create confusion matrix   
    eval_df = pd.concat([pred_s, obs_s], axis = 1)
    eval_df['val'] = 1
    # confusion matrix
    cm = eval_df.pivot_table(index = 'Original', columns = 'Prediction', values = 'val', aggfunc = np.sum)
    
    # print heatmap
    sns.set(font_scale=1)
    plt.figure(figsize = (10,5))
    for c in cm.columns:
        cm[c] = cm[c].fillna(0).astype(int)
    sns.heatmap(cm, linewidths = 0.1, annot = True, fmt = 'd').set_title('Confusion Matrix')

    return(result_dict)

# Description:
# This function takes as input the features and their importances and prints out a barplot of the importance of each feature. It returns a dataframe
# with the importances and the features 
# Input: 
#   importances: a list of importances for each feature. 
#   features: a list of feature names
# Output:
#   eval_df: a dataframe for the features and their importance
def assess_importance(importances, features):
    
    # create importance dataframe
    importance_s = pd.Series(importances, name = 'Importance')
    features_s = pd.Series(features, name = 'Feature')
    eval_df = pd.concat([importance_s, features_s], axis = 1)

    # print importances barplot
    sns.set(font_scale=2)
    plt.figure(figsize = (30, 60))
    sns.barplot(y = 'Feature', x = 'Importance', data = eval_df.sort_values('Importance', ascending = False)) \
        .set_title('Feature Importance')
    return(eval_df)
       
# Description:
# This function analyzes the relationship of the target variable of a regression with each of the categorical variables in a list provided.
# For each level of the categorical variables it computes the mean of the target variable and plots a boxplot. It returns the dataframe of means
# Input: 
#   df: dataframe holding the categorical variables and target
#   categorical_variables: a list of categorical variables 
#   target: dependent variable of regression 
# Output:
#   categorical_means: a dataframe of the means of the target variable for each level of categorical variable
def categorical_regression_analysis(df, categorical_variables, target):
    
    # create averages of target variable for each level of the categorical variable and bind them on a df.
    categorical_means_list = list()
    for c in categorical_variables:
        for l in df[c].unique():
            l_means = df.loc[df[c] == l, target].mean()
            categorical_means_list.append({'variable' : c, 
                                           'label' : l, 
                                           target + '_avg' : l_means})
    categorical_means = pd.DataFrame(categorical_means_list)
    
    sns.set(font_scale=2)
    # plot boxplots of the target variable for each level of the categorical variables
    cats = len(categorical_variables)
    fig, axes = plt.subplots(nrows = cats // 2, ncols = 2, figsize = (30, 40))
    for i in range(cats):
        sns.boxplot(x = categorical_variables[i], y = target, data = df, ax = axes[i // 2, i % 2])
    
    return(categorical_means)
    
# Description:
# This function analyzes the relationship of the target variable of a regression with each of the numeric variables in a list provided.
# For each numeric variable it plots 3 correlation plots. One for the original numeric variable and the target, one for the original 
# variable and the log of the target and finally one where both are loged. Finally it combines all the possible correlations for 
# numeric variable in one dataframe and returns it.
# Input: 
#   df: dataframe holding the categorical variables and target
#   numeric_variables: a list of numeric variables 
#   target: dependent variable of regression  
# Output:
#   correlation_df: a dataframe of the correlations for each numeric variable and the target
def numerical_regression_analysis(df, numeric_variables, target):
    
    local_df = df.copy(deep = True)
    correlation_list = list()
    
    # small amount to add on variables before taking the logarithm
    ets = 1e-15
    
    # log of target_variable
    local_df[target + '_log'] = np.log(local_df[target] + ets)
    # for each numeric variable calculate 3 correlations and plot 3 scatterplots
    for i in range(len(numeric_variables)):
        # log of variable
        local_df[numeric_variables[i] + '_log'] = np.log(local_df[numeric_variables[i]] + ets)
        
        fig, axes = plt.subplots(nrows = 1, ncols = 3, figsize = (20, 7))
        fig.suptitle(numeric_variables[i], fontsize = 30)
        
        # 3 correlations, between original variable and target, log of target and original variable, and logs of both
        corr_0 = local_df[[target, numeric_variables[i]]].corr().loc[target, numeric_variables[i]].round(2)
        corr_1 = local_df[[target + '_log', numeric_variables[i]]].corr().loc[target + '_log', numeric_variables[i]].round(2)
        corr_2 = local_df[[target + '_log', numeric_variables[i] + '_log']].corr().loc[target + '_log', numeric_variables[i] + '_log'].round(2)
        
        # append correlations to a list
        correlation_dict = {'variable' : numeric_variables[i],'original' : corr_0, 'log_target' : corr_1, 'log_both' : corr_2,}
        correlation_list.append(correlation_dict)
        
        # create 3 plots, one for each correlation
        non_zero_index = local_df[target] > 0
        sns.regplot(x = target, y = numeric_variables[i], data = local_df.loc[non_zero_index], ax = axes[0])
        sns.regplot(x = target + '_log', y = numeric_variables[i], data = local_df.loc[non_zero_index], ax = axes[1])
        sns.regplot(x = target + '_log', y = numeric_variables[i] + '_log', data = local_df.loc[non_zero_index], ax = axes[2])
        
        axes[0].set_title('Non Transformed, Corr: ' + str(corr_0), fontsize = 20)
        axes[1].set_title('Loged target, Corr: ' + str(corr_1), fontsize = 20)
        axes[2].set_title('Both loged , Corr: ' + str(corr_2), fontsize = 20)

    # create a dataframe of correlations
    correlations_df = pd.DataFrame(correlation_list)
    correlations_df.set_index('variable', inplace = True)
    return(correlations_df)
    

# Description:
# This function imports a filename (csv) or downloads a table from sql. In both cases it performs a data audit, printing general information for the data,
# checking unique fields and missing values and printing descriptive statistics for the numeric fields or frequencies for the caretgorical. Then it returns the 
# table.
# Input: 
#   filename: filename of the csv or sql query if conn is not None
#   stats: boolean flag on whether it should print statistics for the import. Defaults to True
#   conn: The connection to the sql database. It is needed if the data import is from SQL. Defaults to None
# Output:
#   df: the imported data frame
def data_import(filename, stats = True, conn = None):
    # check conn to understand if it is a sql or csv import
    if conn is not None:
        df = pd.read_sql_query(filename,conn)
    else:   
        # try different encodings in case csv import fails
        try:
            df = pd.read_csv(filename ,encoding = 'utf8')
        except:
            df = pd.read_csv(filename ,encoding = 'cp1252')
    
    # Print general information about data
    print(color.BOLD + '******** General Information ********' + color.END)
    print("Rows: %d Columns: %d " %(df.shape[0], df.shape[1]))
    info = df.info()
    missing = df.isnull().sum()
    if np.sum(missing > 0) > 0:
        print(color.BOLD + 'Missing Values:' + color.END)
        print(color.BLUE)
        print(missing[missing > 0])
        print(color.END)
    else:
        print(color.UNDERLINE + 'No Missing Values' + color.END)
    
    # Check fields for uniqueness
    uniques = df.nunique()
    uniqueness = False
    for i in range(len(uniques)):
        if uniques.iloc[i] == df.shape[0]:
            print(color.BLUE + "Unique on %s" %(uniques.index[i]) + color.END)
            uniqueness = True
    if uniqueness == False:
        print(color.UNDERLINE + 'No unique identifier in the data frame' + color.END)
        
    # print stats for the data
    if stats:
        print(color.BOLD + '******** Stats ********' + color.END)
        print(color.BOLD + '******** Numeric ********'+ color.END)
        print(df.describe())
        print(color.BOLD + '******** Categorical ********'+ color.END)
        cats = df.select_dtypes(include = 'object').columns
        for c in cats:
            print(color.BLUE + '\t %s' % (c) + color.END )
            print('Unique Levels: %d' % (df[c].nunique()))
            print(df[c].value_counts().iloc[:10])
            
    return(df)